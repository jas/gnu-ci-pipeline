# GNU Continous Integration Pipeline

Build and test your GNU-style project using [GitLab
CI/CD](https://docs.gitlab.com/ee/ci/).

We support GNU-like (./configure + make) projects with [gnulib-style
./bootstrap](https://www.gnu.org/software/gnulib/manual/html_node/Developer-tools.html).

# Setup via .gitlab-ci.yml

Create a .gitlab-ci.yml file in your git repository containing the
following:

```yaml
---
include:
  - https://gitlab.org/jas/gnu-ci-pipeline/raw/master/gnu.yml
```

Push your commit and watch CI/CD build and test your project.

# Usage without .gitlab-ci.yml

If you for some reason do not wish to commit a .gitlab-ci.yml file to
your repository (for example because you are CI/CD-testing projects
which you do not maintain) you may chose one of two approaches:

* GitLab CI/CD configuration

Go to `Settings` then `CI/CD` and expand `General pipelines` and under
`CI/CD configuration file` you put `gnu.yml@jas/gnu-ci-pipeline`.  The
disadvantage with this approach is that you cannot configure
per-project variables.

* Separate git repository with CI/CD rules

Create an empty git project to hold CI/CD rules.  To illustrate, let's
say you have a project `jas/foo` (i.e., personal namespace `jas` and
project name `foo`) create `jas/foo-cicd`.  Then put the following in
a file `foo.yml`:

```yaml
---
include:
  - https://gitlab.org/jas/gnu-ci-pipeline/raw/master/gnu.yml
```

Then for the `jas/foo` project go to `Settings` then `CI/CD` and
expand `General pipelines` and under `CI/CD configuration file` you
put `foo.yml@jas/foo-cicd`.

The advantage with this approach is that you can configure per-project
variables.

# Customization

TBW
